<?php

namespace App\Entity;

use App\Repository\CurrencyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CurrencyRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Currency extends Base
{
    /**
     * @ORM\Column(type="string", length=3)
     */
    private string $code;

    /**
     * @ORM\OneToMany(targetEntity=Rate::class, mappedBy="currency", orphanRemoval=true)
     */
    private Collection $rates;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $base;

    public function __construct()
    {
        $this->rates = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function isBase(): bool
    {
        return $this->base;
    }

    public function setBase(bool $base): self
    {
        $this->base = $base;

        return $this;
    }

    /**
     * @return Collection|Rate[]
     */
    public function getRates(): Collection
    {
        return $this->rates;
    }

    public function addRate(Rate $rate): self
    {
        if (!$this->rates->contains($rate)) {
            $this->rates[] = $rate;
            $rate->setCurrency($this);
        }

        return $this;
    }

    public function removeRate(Rate $rate): self
    {
        if ($this->rates->removeElement($rate)) {
            // set the owning side to null (unless already changed)
            if ($rate->getCurrency() === $this) {
                $rate->setCurrency(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->code;
    }
}
