<?php

namespace App\Entity;

use App\Repository\RateRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RateRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Rate extends Base
{
    /**
     * @ORM\ManyToOne(targetEntity=Currency::class, inversedBy="rates")
     * @ORM\JoinColumn(nullable=false)
     */
    private Currency $currency;

    /**
     * @ORM\ManyToOne(targetEntity=TableRate::class, inversedBy="rates")
     * @ORM\JoinColumn(nullable=false)
     */
    private TableRate $tableRate;

    /**
     * @ORM\Column(type="float")
     */
    private float $rate;

    /**
     * Rate constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getRate(): ?string
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getTableRate(): TableRate
    {
        return $this->tableRate;
    }

    public function setTableRate(TableRate $tableRate): self
    {
        $this->tableRate = $tableRate;

        return $this;
    }
}
