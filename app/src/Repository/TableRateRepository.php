<?php

namespace App\Repository;

use App\Entity\TableRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TableRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableRate[]    findAll()
 * @method TableRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableRate::class);
    }

    public function findFirstAvailable(\DateTimeImmutable $date): ?TableRate
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.date <= :date')
            ->andWhere('t.date > :max_diff_date')
            ->andWhere('t.disable = :disable')
            ->setParameter('date', $date)
            ->setParameter('max_diff_date', $date->modify('-5 day'))
            ->setParameter('disable', false)
            ->orderBy('t.date', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
