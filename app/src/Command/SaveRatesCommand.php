<?php

namespace App\Command;

use App\Repository\CurrencyRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SaveRatesCommand extends Command
{
    protected static $defaultName = 'app:save-rates';
    protected static string $defaultDescription = 'Add a short description for your command';

    private CurrencyRepository $currencyRepository;


    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('currency', 'c', InputOption::VALUE_NONE, 'select Currency', 'all')
            ->addOption('');

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
