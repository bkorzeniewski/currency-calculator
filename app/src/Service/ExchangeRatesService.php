<?php

namespace App\Service;

use App\Entity\TableRate;
use App\Repository\TableRateRepository;
use App\Service\ApiClient\ApiClientInterface;
use App\Service\ApiClient\UnavailableDateTableRateException;
use Doctrine\ORM\EntityManagerInterface;

class ExchangeRatesService
{
    private static array $cacheTableRates = [];

    private ApiClientInterface $apiClient;

    private EntityManagerInterface $entityManager;

    private TableRateRepository $tableRateRepository;

    private TableRateModelMapper $tableRateModelMapper;

    public function __construct(
        ApiClientInterface $apiClient,
        EntityManagerInterface $entityManager,
        TableRateRepository $tableRateRepository,
        TableRateModelMapper $tableRateModelMapper
    )
    {
        $this->apiClient = $apiClient;
        $this->entityManager = $entityManager;
        $this->tableRateRepository = $tableRateRepository;
        $this->tableRateModelMapper = $tableRateModelMapper;
    }


    public function saveTableRates(\DateTimeImmutable $date): TableRate
    {
        $count = 0;
        do {
            if ($tableRate = $this->getTableRate($date)) {
                return $tableRate;
            }
            $tableRateIsAvailable = false;
            try {
                $table = $this->apiClient->getTableModel($date);
                $tableRate = $this->tableRateModelMapper->map($table);
                $tableRateIsAvailable = true;
            } catch (UnavailableDateTableRateException $exception) {
                $tableRate = $this->createTableRateDisable($date);
                $date = $date->modify('-1 day');
                $count++;
            }

            $this->entityManager->persist($tableRate);

        } while (!$tableRateIsAvailable && $count < 5);
        $this->entityManager->flush();

        return $tableRate;
    }

    private function getTableRate(\DateTimeImmutable $date): ?TableRate
    {
        $key = $this->getKeyCacheTableRates($date);
        if (array_key_exists($key, static::$cacheTableRates)) {
            return static::$cacheTableRates[$key];
        }
        if ($tableRate = $this->tableRateRepository->findOneByDate($date)) {
            $this->saveCacheTableRate($date, $tableRate);

            return $tableRate;
        }

        return null;
    }

    private function saveCacheTableRate(\DateTimeImmutable $date, TableRate $value): void
    {
        static::$cacheTableRates[$this->getKeyCacheTableRates($date)] = $value;
    }

    private function getKeyCacheTableRates(\DateTimeImmutable $date): string
    {
        return $date->format('Y-m-d');
    }

    private function createTableRateDisable(\DateTimeImmutable $date): TableRate
    {
        $tableRate = new TableRate();
        $tableRate->setDate($date);
        $tableRate->setDisable(true);

        return $tableRate;
    }
}