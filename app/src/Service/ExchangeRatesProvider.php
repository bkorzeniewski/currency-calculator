<?php

namespace App\Service;


use App\Entity\TableRate;
use App\Repository\TableRateRepository;

class ExchangeRatesProvider
{
    private TableRateRepository $tableRateRepository;

    private ExchangeRatesService $exchangeRatesService;

    public function __construct(TableRateRepository $tableRateRepository, ExchangeRatesService $exchangeRatesService)
    {
        $this->tableRateRepository = $tableRateRepository;
        $this->exchangeRatesService = $exchangeRatesService;
    }

    public function getAvailableTableRate(\DateTimeImmutable $date): ?TableRate
    {
        if ($tableRate = $this->tableRateRepository->findFirstAvailable($date)) {
            return $tableRate;
        }

        if ($tableRate = $this->exchangeRatesService->saveTableRates($date)) {
            return $tableRate;
        }

        return null;
    }
}