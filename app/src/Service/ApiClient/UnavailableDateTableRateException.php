<?php


namespace App\Service\ApiClient;


use Throwable;

class UnavailableDateTableRateException extends \RuntimeException
{
    public function __construct(\DateTimeInterface $date, $message = "", $code = 0, Throwable $previous = null)
    {
        $message = $message ?: "Date [{$date->format('Y-m-d')}] is unavailable for Table Rates";
        parent::__construct($message, $code, $previous);
    }
}