<?php

namespace App\Service\ApiClient;

use App\Model\ApiResponse\TableModelInterface;

interface ApiClientInterface
{
    public function getTableModel(\DateTimeInterface $date): TableModelInterface;
}