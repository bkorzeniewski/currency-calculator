<?php


namespace App\Service\ApiClient;

use App\Model\ApiResponse\NBP\TableModel;
use App\Model\ApiResponse\TableModelInterface;
use JMS\Serializer\Serializer;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NBPApiClient implements ApiClientInterface
{
    private HttpClientInterface $client;
    private string $endpoint;
    private LoggerInterface $logger;
    private Serializer $serializer;

    public function __construct(HttpClientInterface $client, string $endpoint, LoggerInterface $logger, Serializer $serializer)
    {
        $this->client = $client;
        $this->endpoint = $endpoint;
        $this->logger = $logger;
        $this->serializer = $serializer;
    }


    public function getTableModel(\DateTimeInterface $date): TableModelInterface
    {
        $data = $this->getResponseData($date);

        return $this->serializer->fromArray($data, TableModel::class);
    }

    private function getResponseData(\DateTimeInterface $date): ?array
    {
        $url = "{$this->endpoint}/{$date->format('Y-m-d')}?format=json";
        $response = $this->client->request('GET', $url);
        try {
            $data = $response->toArray();
        } catch (ClientExceptionInterface $exception) {
            $this->logger->error($exception->getMessage(), ['exception' => $exception]);
            throw new UnavailableDateTableRateException($date, "", $exception->getCode(), $exception);
        }

        return current($data);
    }
}