<?php


namespace App\Service;

use App\Entity\Currency;
use App\Entity\Rate;
use App\Entity\TableRate;
use App\Model\ConvertViewModel;
use App\Model\ExchangeRatesFormModel;

class ExchangeRatesConverter
{
    private ExchangeRatesProvider $exchangeRatesProvider;

    public function __construct(ExchangeRatesProvider $exchangeRatesProvider)
    {
        $this->exchangeRatesProvider = $exchangeRatesProvider;
    }

    public function convert(ExchangeRatesFormModel $exchangeRatesModel): ConvertViewModel
    {
        $tableRate = $this->exchangeRatesProvider->getAvailableTableRate($exchangeRatesModel->getDate());
        $baseCurrencyRate = $this->getCurrencyRate($exchangeRatesModel->getBaseCurrency(), $tableRate);
        $targetCurrencyRate = $this->getCurrencyRate($exchangeRatesModel->getTargetCurrency(), $tableRate);
        $result = $exchangeRatesModel->getAmount() * $baseCurrencyRate;
        $resultAmount = $result / $targetCurrencyRate;

        return new ConvertViewModel($exchangeRatesModel, round($resultAmount,2), $tableRate->getDate());
    }

    private function getCurrencyRate(Currency $currency, TableRate $tableRate): ?float
    {
        if ($currency->isBase()) {
            return 1.0;
        }

        $rates = $tableRate->getRates()->filter(function (Rate $rate) use ($currency) {
            return $rate->getCurrency() === $currency;
        });

        if ($rates->isEmpty()) {
            return null;
        }

        return $rates->first()->getRate();
    }

}