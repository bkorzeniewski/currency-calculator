<?php


namespace App\Service;


use App\Entity\Currency;
use App\Entity\Rate;
use App\Entity\TableRate;
use App\Model\ApiResponse\RateModelInterface;
use App\Model\ApiResponse\TableModelInterface;
use App\Repository\CurrencyRepository;

class TableRateModelMapper
{
    private CurrencyRepository $currencyRepository;

    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    public function map(TableModelInterface $tableModel): TableRate
    {
        $currencyCodes = $this->getCurrencyCodes();
        $tableRate = new TableRate();
        $tableRate->setDate($tableModel->getDate());
        foreach ($tableModel->getRates() as $rateModel) {
            /** @var RateModelInterface $rateModel */
            $currencyCode = $rateModel->getCurrencyCode();
            if (!in_array($currencyCode, $currencyCodes, true)) {
                continue;
            }

            $rate = new Rate();
            $rate->setRate($rateModel->getRate());
            $rate->setCurrency($this->currencyRepository->findOneByCode($currencyCode));
            $tableRate->addRate($rate);
        }

        return $tableRate;
    }

    private function getCurrencyCodes(): array
    {
        return array_map(function (Currency $currency) {
            return $currency->getCode();
        }, $this->currencyRepository->findAll());
    }
}