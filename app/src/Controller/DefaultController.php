<?php

namespace App\Controller;

use App\Entity\TableRate;
use App\Form\ExchangeRatesType;
use App\Model\ExchangeRatesFormModel;
use App\Service\ExchangeRatesConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private ExchangeRatesConverter $exchangeRatesConverter;

    public function __construct(ExchangeRatesConverter $exchangeRatesConverter)
    {
        $this->exchangeRatesConverter = $exchangeRatesConverter;
    }

    /**
     * @Route("/", name="default")
     */
    public function index(Request $request): Response
    {
        $model = new ExchangeRatesFormModel();
        $form = $this->createForm(ExchangeRatesType::class, $model);
        $form->handleRequest($request);

        $convertViewModel = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $convertViewModel = $this->exchangeRatesConverter->convert($model);
        }

        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
            'convertViewModel' => $convertViewModel
        ]);
    }

}
