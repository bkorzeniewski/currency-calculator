<?php

namespace App\DataFixtures;

use App\Entity\Currency;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private const BASE_CURRENCY = 'PLN';

    private const CURRENCIES = ['PLN', 'EUR', 'USD'];

    public function load(ObjectManager $manager)
    {
        foreach (static::CURRENCIES as $code) {
            $currency = new Currency();
            $currency->setCode($code);
            $currency->setBase(static::BASE_CURRENCY === $code);
            $manager->persist($currency);
        }

        $manager->flush();
    }
}
