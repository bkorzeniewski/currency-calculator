<?php

namespace App\Model;

use App\Entity\Currency;

class ExchangeRatesFormModel
{
    private float $amount;

    private Currency $baseCurrency;

    private Currency $targetCurrency;

    private \DateTimeImmutable $date;

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    public function getBaseCurrency(): Currency
    {
        return $this->baseCurrency;
    }

    public function setBaseCurrency(Currency $baseCurrency): void
    {
        $this->baseCurrency = $baseCurrency;
    }

    public function getTargetCurrency(): Currency
    {
        return $this->targetCurrency;
    }

    public function setTargetCurrency(Currency $targetCurrency): void
    {
        $this->targetCurrency = $targetCurrency;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): void
    {
        $this->date = $date;
    }

}