<?php

namespace App\Model;

class ConvertViewModel
{
    private ExchangeRatesFormModel $exchangeRatesFormModel;

    private float $resultAmount;

    private \DateTimeImmutable $date;

    public function __construct(ExchangeRatesFormModel $exchangeRatesFormModel, float $resultAmount, \DateTimeImmutable $date)
    {
        $this->exchangeRatesFormModel = $exchangeRatesFormModel;
        $this->resultAmount = $resultAmount;
        $this->date = $date;
    }

    public function getExchangeRatesModel(): ExchangeRatesFormModel
    {
        return $this->exchangeRatesFormModel;
    }

    public function getResultAmount(): float
    {
        return $this->resultAmount;
    }

    public function isDateDisabled(): bool
    {
        return $this->exchangeRatesFormModel->getDate() !== $this->date;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }
}