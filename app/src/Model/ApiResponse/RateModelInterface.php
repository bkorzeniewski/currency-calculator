<?php

namespace App\Model\ApiResponse;

interface RateModelInterface
{
    public function getCurrencyCode(): string;

    public function getRate(): float;
}