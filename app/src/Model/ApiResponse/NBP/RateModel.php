<?php

namespace App\Model\ApiResponse\NBP;

use App\Model\ApiResponse\RateModelInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class RateModel implements RateModelInterface
{
    /**
     * @Serializer\Expose
     * @Serializer\Type("string")
     */
    private string $code;

    /**
     * @Serializer\Expose
     * @Serializer\Type("float")
     */
    private float $mid;

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getMid(): float
    {
        return $this->mid;
    }

    public function setMid(float $mid): void
    {
        $this->mid = $mid;
    }

    public function getCurrencyCode(): string
    {
        return $this->code;
    }

    public function getRate(): float
    {
        return $this->mid;
    }
}