<?php

namespace App\Model\ApiResponse\NBP;

use App\Model\ApiResponse\TableModelInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class TableModel implements TableModelInterface
{
    /**
     * @Serializer\Expose
     * @Serializer\SerializedName("effectiveDate")
     * @Serializer\Type("DateTimeImmutable<'Y-m-d'>")
     */
    private \DateTimeImmutable $effectiveDate;

    /**
     * @Serializer\Expose
     * @Serializer\Type("array<App\Model\ApiResponse\NBP\RateModel>")
     * @var array
     */
    private array $rates;

    public function getEffectiveDate(): \DateTimeImmutable
    {
        return $this->effectiveDate;
    }

    public function setEffectiveDate(\DateTimeImmutable $effectiveDate): void
    {
        $this->effectiveDate = $effectiveDate;
    }

    public function setRates(array $rates): void
    {
        $this->rates = $rates;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->effectiveDate ?? new \DateTimeImmutable();
    }

    public function getRates(): array
    {
        return $this->rates;
    }
}