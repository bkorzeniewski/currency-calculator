<?php


namespace App\Model\ApiResponse;


/**
 * @author Błażej Korzeniewski <blazej.korzeniewski@xtb.com>
 */
interface TableModelInterface
{
    public function getDate(): \DateTimeImmutable;

    public function getRates(): array;
}